<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
    <xsl:template match="/">
        <html>
            <head>
                 <!-- <link rel='stylesheet' type='text/css' media='screen' href='css/main.css'/> -->
            </head>
            <body>
                <h1>Entradas Vehiculos</h1>
                <hr/>
                <xsl:apply-templates select="Entradas" />
            </body>
        </html> 
    </xsl:template> 

    <xsl:template match="Entradas">
         <table>
            <tr>
                <td>
                    Deposito
                </td>
                <td>
                    motivo
                </td>
                <td>
                    fechaEntrada
                </td>
                <td>
                    direccion
                </td>
            </tr>

            <xsl:for-each select="./Vehiculo">
                <tr>
                    <td>
                        <xsl:value-of select="deposito"/>
                    </td>
                    <td>
                        <xsl:value-of select="motivo"/>
                    </td>
                    <td>
                        <xsl:value-of select="fechaEntrada"/>
                    </td>
                    <td>
                        <xsl:value-of select="tipoVia"/> &nbps;&nbps; <xsl:value-of select="via"/> <xsl:value-of select="numero"/>, <xsl:value-of select="cp"/>
                    </td>
                </tr>                
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>
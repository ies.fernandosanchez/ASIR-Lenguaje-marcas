<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <head>
    <style>
        table{
            width: 100%;
            text-align: center;
            margin-bottom: 10px;
        }
    </style>
  </head>
  <body>
    <h2>Centros educativos</h2>

    <!-- <xsl:variable name="headerCentro">
        <tr>
            <td colspan="2">
                <xsl:value-of select="./@nombreCentro"/>
            </td>
        </tr>
    </xsl:variable> -->

    <xsl:for-each select="centros/centro">
        <table border="1">

            <tr>
                <td colspan="2">
                    <xsl:value-of select="./@nombreCentro"/>
                </td>
            </tr>
            <!-- <xsl:copy-of select="$headerCentro"/> -->
            <!-- <xsl:value-of select="$headerCentro"/> -->
            <xsl:for-each select="aula">
                <tr>
                    
                    <!-- <xsl:if test="./@estado='Libre'">
                        <td style="background: #0f0">
                            <xsl:value-of select="./@nombre"/>
                        </td>
                    </xsl:if>

                    <xsl:if test="./@estado='Ocupada'">
                        <td style="background: #f00">
                            <xsl:value-of select="nombre"/>
                        </td>
                    </xsl:if>  -->
                   
                    
                    <xsl:choose>
                        <xsl:when test="./@estado='Libre'">
                            <td style="background: #0f0">
                                <xsl:value-of select="nombre"/>
                            </td>
                        </xsl:when>
                        <xsl:otherwise>
                            <td style="background: #f00">
                                <xsl:value-of select="nombre"/>
                            </td>
                        </xsl:otherwise>
                    </xsl:choose>

                    <td>
                        <table border="0">
                            <xsl:for-each select="inventario/pc">
                                <tr>
                                    <td><xsl:value-of select="./@id"/></td>
                                </tr>
                            </xsl:for-each>
                        </table>
                    </td>                    
                </tr>
            </xsl:for-each>
        </table>
    </xsl:for-each>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>


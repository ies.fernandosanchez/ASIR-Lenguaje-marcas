<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link rel='stylesheet' type='text/css' media='screen' href='main.css'/>
            </head>
            <body>
                <h1>Todas las recetas</h1>
                <xsl:for-each select="recetas/receta">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="receta">
        <div class="receta">
            <h2>
                <xsl:value-of select="./@nombre"/>
            </h2>
            <p>
            Descripcion: 
                <xsl:value-of select="./descripcion"/>
            </p>
        </div>
    </xsl:template>
</xsl:stylesheet>
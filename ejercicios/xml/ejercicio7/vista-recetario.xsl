<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link 
                    rel='stylesheet' 
                    type='text/css' 
                    media='screen' 
                    href='css/main.css'/>
            </head>
            <body>
                <h1>
                    Todas las recetas
                </h1>

                <xsl:for-each select="recetario/receta">
                    <div class="receta">
                        <h2>
                            <xsl:value-of select="./@nombre"/>
                        </h2>
                        <p>
                            <xsl:value-of select="./@summary"/>
                        </p>
                        
                        <xsl:element name="a">
                            <xsl:attribute name="href">
                                <xsl:value-of select="./@file"/>
                            </xsl:attribute>
                            <xsl:attribute name="class">btn btn-success</xsl:attribute>
                            VER
                        </xsl:element>


                        <xsl:element name="a">
                            <xsl:attribute name="href">
                                <xsl:value-of select="./@file"/>
                            </xsl:attribute>
                            <xsl:attribute name="class">btn btn-danger</xsl:attribute>
                            ELIMINAR
                        </xsl:element>
                    </div>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
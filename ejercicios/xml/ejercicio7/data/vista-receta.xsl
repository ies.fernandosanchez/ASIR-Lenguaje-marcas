<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link rel='stylesheet' type='text/css' media='screen' href='../css/main.css'/>
            </head>
            <body>
                <h1>
                    <xsl:value-of select="receta/@nombre"/>
                </h1>

                <xsl:element name="img">
                    <xsl:attribute name="src">
                        <xsl:value-of select="receta/@image"/>
                    </xsl:attribute>
                    <xsl:attribute name="class">image</xsl:attribute>
                </xsl:element>

                
                <xsl:apply-templates select = "receta/ingredientes" />
                <ol>
                    <xsl:for-each select="receta/procesos/proceso">
                        <li>
                            <xsl:value-of select="."/>
                        </li>
                    </xsl:for-each>
                </ol>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="ingredientes">
        <ul>
            <xsl:for-each select="./ingrediente">
                <li>
                    <xsl:value-of select="."/>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
</xsl:stylesheet>
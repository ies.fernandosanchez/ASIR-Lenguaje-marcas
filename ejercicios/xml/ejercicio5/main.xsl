<?xml version="1.0" encoding="UTF-8"?>
 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
    <xsl:template match="/">
        <html>
            <head>
                 <link rel='stylesheet' type='text/css' media='screen' href='css/main.css'/>
            </head>
            <body>
                <h1>Lista de Javier</h1>
                <br/>
                <hr/>
                <xsl:apply-templates select="personas" />
            </body>
        </html> 
    </xsl:template>
 

    <xsl:template match="personas">
        <xsl:for-each select="./persona">
            <!-- <xsl:value-of select="."/> -->
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>
 
    <xsl:template match="persona">
            <h4><xsl:value-of select="nombre"/></h4>
            <b><xsl:value-of select="estadoCivil"/></b><br/>
            <h5><xsl:value-of select="fechaNacimiento"/></h5>
            <div class="tabla">
                <xsl:apply-templates select="padres"/>
            </div>

            <div style="background:#cacaca; margin-top:10px;">
                <xsl:apply-templates select="hijos"/>
            </div>
    </xsl:template>
 
    <xsl:template match="padres">
        <h3>Padres:</h3>
        <ul>
            <li><xsl:value-of select="madre"/></li>        
            <li><xsl:value-of select="padre"/></li>
        </ul>
    </xsl:template>
 
    <xsl:template match="hijos">
        <h3>Hijos</h3>
        <ul>        
            <xsl:for-each select="./hijo">
                <li><xsl:apply-templates select="."/></li>
            </xsl:for-each>
        </ul>
    </xsl:template>
 
    <xsl:template match="hijo">
        <xsl:value-of select="nombre"/> 
        <b> ==></b>
        <xsl:value-of select="fechaNacimiento"/>
    </xsl:template>
 
</xsl:stylesheet>
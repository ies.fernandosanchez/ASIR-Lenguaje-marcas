<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
        <head>
        
            <title>Ejemplo XSLT</title>
            
            <style>
                table{
                    width:100%;
                }
            </style>
        </head>
        <body>
            <h1>Ciudades de españa</h1>

            <table border="1">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Habitantes</th>
                    </tr>
                </thead>
                <xsl:for-each select="ciudades/ciudad">
                    <tr>
                        <td>
                            <xsl:value-of select="nombre">
                            </xsl:value-of>
                        </td>
                        <td>
                            <xsl:value-of select="habitantes"/>
                        </td>
                        <!-- <xsl:apply-templates select="nombre" />
                        <xsl:apply-templates select="habitantes" /> -->
                    </tr>
                </xsl:for-each>
            </table>
        </body>
        </html>
    </xsl:template>

    <xsl:template match="nombre">
        <td>
            <xsl:value-of select="."/>
        </td>
    </xsl:template>
    <xsl:template match="habitantes">
        <td>
            <xsl:value-of select="."/>
        </td>
    </xsl:template>
</xsl:stylesheet>
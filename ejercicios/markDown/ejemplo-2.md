---
title: "Ejemplo Uso de RMarkdown"
author: "fer"
date: '\today'
---

<link rel='stylesheet' href='css/main.css'/>



# Título 1 (nivel de mayor jerarquía)
## Título 2 (nivel de segunda jerarquía)
### Título 3 (nivel de tercera jerarquía)



1. intro
2. tablas
3. texto

4. [ejemplo](#ejemplo)


<div style="page-break-after: always;"></div>

<p class="special_table"></p>

| First Header  | Second Header |
|             - |             - |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |

- dadaf
    - fdfsg
        - fsf
            - fsf
                - fdfaef
                1. fdsafadf
                1. fdfsg

<div class="blue_table"></div>

| First Header  | Second Header |
| ------------- | ------------- |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |




## <p id="ejemplo">4. Ejemplo</p>
blablabablabla </br>csfcsscc          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sggdasg &
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<head>
				<link rel='stylesheet' type='text/css' media='screen' href='bootstrap-5.1.3-dist/css/bootstrap.css'/>
				<script src='bootstrap-5.1.3-dist/js/bootstrap.js'></script>
			</head>
			<body>
				<div class="row">
					<div class="col-6"></div>
					<div class="col-6">
						<div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
							<div class="carousel-indicators">
								<xsl:for-each select="casas/casa/imagenes/imagen">
									<xsl:element name="button">
										<xsl:attribute name="type">                                    button                                </xsl:attribute>
										<xsl:attribute name="data-bs-target">                                    #myCarousel                                </xsl:attribute>
										<xsl:attribute name="data-bs-slide-to">
											<xsl:value-of select="position()-1"/>
										</xsl:attribute>
										<xsl:attribute name="class">
											<xsl:if test="position()=1">                                        active                                    </xsl:if>
										</xsl:attribute>
									</xsl:element>
								</xsl:for-each>
							</div>
							<div class="carousel-inner">
								<xsl:for-each select="casas/casa/imagenes/imagen">
									<xsl:element name="div">
										<xsl:attribute name="class">                                    carousel-item                                    
											<xsl:if test="position()=1">                                        active                                    </xsl:if>
										</xsl:attribute>
										<xsl:element name="img">
											<xsl:attribute name="src">                                        img/
												<xsl:value-of select="path"/>
											</xsl:attribute>
											<xsl:attribute name="class">                                        bd-placeholder-img                                    </xsl:attribute>
											<xsl:attribute name="width">                                        100%                                    </xsl:attribute>
											<xsl:attribute name="height">                                        100%                                    </xsl:attribute>
										</xsl:element>
									</xsl:element>
								</xsl:for-each>
							</div>
							<button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Previous</span>
							</button>
							<button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Next</span>
							</button>
						</div>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
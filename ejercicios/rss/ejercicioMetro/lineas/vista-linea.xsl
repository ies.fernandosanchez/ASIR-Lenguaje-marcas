<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link 
                    rel='stylesheet' 
                    type='text/css' 
                    media='screen' 
                    href='css/main.css'/>
            </head>
            <body>             
<!-- 
<h1 style="background:<xsl:value-of select='linea/color'/>">
 <xsl:value-of select="linea/@name"/>
</h1> -->
                <xsl:element name="h1">
                    <xsl:attribute name="style">
                        background:<xsl:value-of select="linea/color"/>
                    </xsl:attribute>
                    <xsl:value-of select="linea/@name"/>
                </xsl:element>

                <div id="estaciones_linea">
                    <ul>
                        <xsl:for-each select="linea/estaciones/estacion">

                            <li>
                                <xsl:value-of select="./name"/>
                            </li>
                        </xsl:for-each>
                    </ul>
                </div>



                <!-- <h1 style="background: ">
                    <xsl:value-of select="linea/@name"/>
                </h1> -->

                

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
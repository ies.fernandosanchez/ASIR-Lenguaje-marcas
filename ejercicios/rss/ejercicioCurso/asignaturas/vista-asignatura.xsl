<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head> 
                <style>

h2 {
    text-align: center;
}

.profesor {
    width: 50%;
    display: inline-block;
    float: left;
}

img{
    width:100%;
}

ul {
    list-style: none;
}

.temario {
    width: 47%;
    display: inline-block;
    position: relative;
    top: 0px;
    background: #ccc;
    margin-left: 0.5%;
    margin-right: 0.5%;
    padding: 50px 0px 50px 15px;
    max-width: 48%;
    text-align: center;
    line-height: 45px;
    /* list-style: none; */
    font-size: 20px;
    font-weight: 600;
    border-radius: 13px;
    border: 2px solid #000;
    box-shadow: 20px 20px 20px 5px;
}
h1 {
    text-align: center;
    font-size: 80px;
    margin-bottom: 0px;
}



#temario {
    position:absolute;
    right:0px;
    width:100%;  
    -webkit-transition: all 1s;
    -moz-transition: all 1s;
    -mos-transition: all 1s;
    -o-transition: all 1s;
    transition: all 1s;
}
#temario {
    right:-1000px;
}

                </style>              
            </head>
            <body>      
                <h1>
                    <xsl:value-of select="asignatura/@name"/>
                </h1> 
                <div class="profesor">
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="asignatura/profesor/img"/>
                        </xsl:attribute>

                    </xsl:element>
                    <h2>
                        <xsl:value-of select="asignatura/profesor/name"/>
                    </h2>
                </div>
                <div class="temario">
                    <ul>
                        <xsl:for-each select="asignatura/temario/tema">
                            <li>
                                <xsl:value-of select="./name"/>
                            </li>
                        </xsl:for-each>
                    </ul>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>